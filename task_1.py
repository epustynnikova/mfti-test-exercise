from functools import reduce
from typing import List

import hail as hl
from hail import MatrixTable

DEFAULT_REFERENCE_GENOME: str = 'GRCh38'


def load_matrix(paths: List[str], reference_genome=DEFAULT_REFERENCE_GENOME) -> MatrixTable:
    return reduce(lambda left, right: left.union_cols(right, row_join_type='outer'),
                  [hl.import_vcf(path, reference_genome=reference_genome) for path in paths])


if __name__ == '__main__':
    load_matrix(['data/HG002_GRCh38_1_22_v4.2.1_benchmark.vcf',
                 'data/HG003_GRCh38_1_22_v4.2.1_benchmark.vcf',
                 'data/HG004_GRCh38_1_22_v4.2.1_benchmark.vcf'])
