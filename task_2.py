from functools import reduce
from typing import List

import hail as hl
from hail import MatrixTable

from task_1 import load_matrix

DEFAULT_REFERENCE_GENOME: str = 'GRCh38'


def find_samples_by_rs_ids(mt: MatrixTable, rs_ids: List[str], reference_genome=DEFAULT_REFERENCE_GENOME) -> List[str]:
    return get_samples(reduce(lambda left, right: left.union_cols(right, row_join_type='outer'),
                              [mt.filter_rows(mt.rsid == rs_id, keep=False, reference_genome=reference_genome)
                               for rs_id in rs_ids]))


def find_samples_by_intervals(mt: MatrixTable, intervals: List[str], reference_genome=DEFAULT_REFERENCE_GENOME) \
        -> List[str]:
    expressions = [hl.parse_locus_interval(interval, reference_genome=reference_genome) for interval in intervals]
    return get_samples(hl.filter_intervals(mt, expressions))


def get_samples(mt: MatrixTable) -> List[str]:
    all_samples = mt.s.take(n=mt.count_cols())
    samples = list()
    entries = mt.entry.take(mt.count_cols() * mt.count_rows())
    for idx, entry in enumerate(entries):
        sample = all_samples[idx % len(all_samples)]
        if entry.DP is not None and sample not in samples:
            samples.append(sample)
        if all_samples.__len__() == len(samples):
            break
    return samples


if __name__ == '__main__':
    input_mt = load_matrix(['data/HG002_GRCh38_1_22_v4.2.1_benchmark.vcf',
                            'data/HG003_GRCh38_1_22_v4.2.1_benchmark.vcf',
                            'data/HG004_GRCh38_1_22_v4.2.1_benchmark.vcf'])
    print(find_samples_by_intervals(input_mt, ['chr1:602113-chr1:602114']))
