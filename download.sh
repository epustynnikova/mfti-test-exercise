#!/bin/sh
mkdir -p data
cd data
wget https://ftp-trace.ncbi.nlm.nih.gov/giab/ftp/release/AshkenazimTrio/HG002_NA24385_son/latest/GRCh38/HG002_GRCh38_1_22_v4.2.1_benchmark.vcf.gz
wget https://ftp-trace.ncbi.nlm.nih.gov/giab/ftp/release/AshkenazimTrio/HG003_NA24149_father/latest/GRCh38/HG003_GRCh38_1_22_v4.2.1_benchmark.vcf.gz
wget https://ftp-trace.ncbi.nlm.nih.gov/giab/ftp/release/AshkenazimTrio/HG004_NA24143_mother/latest/GRCh38/HG004_GRCh38_1_22_v4.2.1_benchmark.vcf.gz
gunzip -k -f HG002_GRCh38_1_22_v4.2.1_benchmark.vcf.gz
gunzip -k -f HG003_GRCh38_1_22_v4.2.1_benchmark.vcf.gz
gunzip -k -f HG004_GRCh38_1_22_v4.2.1_benchmark.vcf.gz