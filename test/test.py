import sys
from task_1 import load_matrix
from task_2 import find_samples_by_intervals
from task_3 import add_vcf


def test_task_1():
    # given":
    files = [f'{sys.path[0]}/data/HG002.vcf', f'{sys.path[0]}/data/HG003.vcf', f'{sys.path[0]}/data/HG004.vcf']

    # when:
    mt = load_matrix(files)

    # when:

    assert mt.count_cols() == 3
    assert mt.s.take(n=mt.count_cols()) == ['HG002', 'HG003', 'HG004']


def test_task_2():
    # given:
    mt = load_matrix([f'{sys.path[0]}/data/HG002.vcf', f'{sys.path[0]}/data/HG003.vcf', f'{sys.path[0]}/data/HG004.vcf'])
    intervals = ['chr1:602113-chr1:602114']

    # when:
    found_samples = find_samples_by_intervals(mt, intervals)

    # then:
    assert found_samples == ['HG002', 'HG004']


def test_task_3():
    # given:
    mt = load_matrix([f'{sys.path[0]}/data/HG002.vcf'])
    new_vcf = f'{sys.path[0]}/data/HG004.vcf'

    # when:
    new_mt = add_vcf(mt, new_vcf)

    # then:
    assert mt.count_cols() == 1
    assert mt.s.take(n=mt.count_cols()) == ['HG002']
    assert new_mt.count_cols() == 2
    assert new_mt.s.take(n=new_mt.count_cols()) == ['HG002', 'HG004']
