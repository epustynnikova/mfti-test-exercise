import hail as hl
from hail import MatrixTable

from task_1 import load_matrix

DEFAULT_REFERENCE_GENOME: str = 'GRCh38'


def add_vcf(mt: MatrixTable, path_to_vcf: str, reference_genome=DEFAULT_REFERENCE_GENOME) -> MatrixTable:
    return mt.union_cols(hl.import_vcf(path_to_vcf, reference_genome=reference_genome))


if __name__ == '__main__':
    input_mt = load_matrix(['data/HG002_GRCh38_1_22_v4.2.1_benchmark.vcf',
                            'data/HG003_GRCh38_1_22_v4.2.1_benchmark.vcf'])
    print(input_mt.s.take(n=input_mt.count_cols()))
    input_mt = add_vcf(input_mt, 'data/HG004_GRCh38_1_22_v4.2.1_benchmark.vcf')
    print(input_mt.s.take(n=input_mt.count_cols()))
