# Mfti Test Exercise

## Задание 1
Очевидно, не самый оптимальный способ, но с примерами по [ссылке](https://ftp-trace.ncbi.nlm.nih.gov/giab/ftp/release/AshkenazimTrio/) способ с combiner'ом не сработал.
Предположительно дело в файлах, как из них сделать готовые g.vcf я не поняла.
Возможно, решение с combiner'ом должно выглядеть как-то так:
```python
import hail as hl

gvcfs = ['HG002.g.vcf.gz', 'HG003.g.vcf.gz', 'HG003.g.vcf.gz']
combiner = hl.vds.new_combiner(
         output_path='data/dataset.vds',
         temp_path='/tmp/',
         gvcf_paths=gvcfs,
         use_genome_default_intervals=True,
         reference_genome='GRCh38')

combiner.run()

vds = hl.vds.read_vds('data/dataset.vds')
```

## Задание 2
Так же очевидно поиск по отфильтрованной matrix table с помощью отдельного метода немного колхозный, но лучше не придумала.

С поиском по rs_id -- работает лишь формально. Предполагаю, что в том, что в файлах не присутствует информация о rs.
Проверяла на [rs12948217](https://www.snpedia.com/index.php/Rs12948217), что присутствует у образца HG002.

Варианты решения:
* обогатить vcf-файлы информацией о rs
* найти сервис однозначного перевода rs в интервал (например, воспользоваться snpedia)

## Задание 3
Дополнений нет.

# Предложения по оптимизации:
Сама библиотека hail довольно быстро осуществляет агрегирование наборов данных, так что основная просадка по скорости наблюдается при чтении самих файлов.
Так что можно разделить файлы с одним пациентом по хромосомам или минимальному размеру файла, ниже которого накладные расходы будут нивелировать уменьшение размера файла, запустить чтение параллельно и агрегировать конечный результат с помощью union_rows().
Пример решения (по хромосомам)
- разбить файлы на менее крупные
```commandline
for file in ls data/*.vcf
do
    for i in {1..22}
    mkdir -p file
    do
        grep '^chr$i' file > 'file/chr$i_file'
    done
done
```
- читать файлы параллельно с помощью multiprocessing.Pool и объединить их в итоговую MatrixTable
```python
from multiprocessing import Pool
import hail as hl
from functools import reduce

def read(path: str) -> hl.MatrixTable:
    return hl.import_vcf(path)

def read_file(filename: str) -> hl.MatrixTable:
    pool = Pool(processes=22)
    processes = []
    for index in range(1, 22):
        processes.append(pool.apply_async(read, [f"{file_name}/chr{index}_file1.vcf"]))
        
    pool.close()
    pool.join()
    return reduce(lambda x,y: x.union_rows(y), [process.get() for process in processes])

def main():
    file_names = ['file1', 'file2', 'file3']
    mt = reduce(lambda x, y: x.union_cols(y), [read_file(file_name) for file_name in file_names])
    mt.describe()
```